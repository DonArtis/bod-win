# ARK Windows Server

## Symlinking ARK Cluster to network drive

If your clustershare is a networkdrive its necessary at first to create a symlink and then a junction between symlink and ARK cluster folder

### Junction to networkdrive

```
new-item -itemtype symboliclink  -path C:\Users\Administrator\ -name bodcluster -value Z:\
```

### Symlink to ARK cluster share

```
new-item -itemtype junction  -path C:\Users\Administrator\ARK\clusters\ -name bodcluster -value C:\Users\Administrator\bodcluster
```

## Captialism Mod

## Symlink for Capitalism Mod (CCIM and Bank2)

```
new-item -itemtype symboliclink -path C:\Users\Administrator\ARK\Servers\bod-amissa001\ShooterGame\Saved\SavedArks\ -name bod_capitalism -value C:\Users\Administrator\ARK\clusters\bod_ark_cluster\bod_capitalism
```

### CCMI settings

```ini
[Capitalism]
CCMICustomServerName=theisland
CCMICustomSaveGameLocation=../SavedArks/bod_capitalism/CCMI/
DeactivateSupplyAndDemand=true
CCMISpoilingTimeMulti=10
CCMIMarketFeePercentage=0.05
CCMIDeliveryCostsPerThousand=0.05
CCMIMaxAssetStorageDurationInSeconds=604800
CCMIAssetBrowserSpoilTimeMulti=3
CCMIMaxAllowedPlayerContracts=100
NPCStructuresUseStructureLimits=true
NPCStructuresPlayerStructureLimit=1
NPCStructuresTribeStructureLimit=2
NPCStructuresServerStructureLimit=200
NPCTraderUseStructureLimits=true
NPCTraderPlayerStructureLimit=0
NPCTraderTribeStructureLimit=0
NPCTraderServerStructureLimit=200
Bank2UseStructureLimits=true
Bank2PlayerStructureLimit=0
Bank2TribeStructureLimit=0
Bank2ServerStructureLimit=200
DinoGarageUseStructureLimits=true
DinoGaragePlayerStructureLimit=1
DinoGarageTribeStructureLimit=1
DinoGarageServerStructureLimit=200
DisableDinoCharacterFoodConsumption=true
```

### Bank 2

Adjust given settings in Game.ini

```ini
[Capitalism.Bank]
UseCustomSavegamePath=../SavedArks/bod_capitalism/
ResetAccountOnDeathEvent=false
OnlyAllowWithdraw=false
InterestCycleSeconds=604800
DisableTribeAccountInterest=true
DisableTribeAccountCredit=false
DisableTribeAccountInitValue=false
InactiveArchiveSeconds=-1
PreventNegativeAccountArchiving=false
AutomaticVotePayoutReason=Vote Coins Payout
BankCurrencyEntry=(CurrencyClassPath=/Game/Mods/CapitalismCurrency/GloryCoin/PrimalItemResource_GloryCoin.PrimalItemResource_GloryCoin,AlternateCurrencyName=,PositiveInterest=0.01,NegativeInterest=0.075,DiminishingThreshold=100000,DiminishingValue=0.25,InitValue=0,CreditMultiplier=0.01,ExchangeWeight=1.0,ExchangeSpread=0.01)
BankCurrencyEntry=(CurrencyClassPath=/Game/PrimalEarth/CoreBlueprints/Resources/PrimalItemResource_Element.PrimalItemResource_Element,AlternateCurrencyName=,PositiveInterest=-1,NegativeInterest=-1,DiminishingThreshold=-1,DiminishingValue=-1,InitValue=-1,CreditMultiplier=-1,ExchangeWeight=-1,ExchangeSpread=-1)
```

### Super Structures

Gameusersettings.ini

```ini
[SuperStructures]
DisableDinoScan=true
MutatorModeBlacklist=AllowBreeding,AgeFreeze,GenderAssign,GenderSwap,Corrupt,Aberrant,XCreature
BinocsDefaultZoom=2
AllowIntakeToPlaceWithoutWater=true
AllowAdminToDemoAnyTribeWithDemoGun=true
EnableWildDinoStr=1
EnableWildDinoStam=1
EnableWildDinoOxygen=1
EnableWildDinoFood=1
EnableWildDinoWeight=1
EnableWildDinoSpeed=1
EnableTameDinoStr=1
EnableTameDinoStam=1
EnableTameDinoOxygen=1
EnableTameDinoFood=1
EnableTameDinoWeight=1
EnableTameDinoSpeed=1
EnableWildLevels=1
HUDDelay=3
PullResourceAdditions=/Game/Extinction/CoreBlueprints/Resources/PrimalItemResource_BlueSap.PrimalItemResource_BlueSap,/Game/Extinction/CoreBlueprints/Resources/PrimalItemResource_CondensedGas.PrimalItemResource_CondensedGas,/Game/Extinction/CoreBlueprints/Resources/PrimalItemResource_CorruptedPolymer.PrimalItemResource_CorruptedPolymer,/Game/Extinction/CoreBlueprints/Resources/PrimalItemResource_ElementDustFromShards.PrimalItemResource_ElementDustFromShards,/Game/Extinction/CoreBlueprints/Resources/PrimalItemResource_ElementDust.PrimalItemResource_ElementDust,/Game/Extinction/CoreBlueprints/Resources/PrimalItemResource_FracturedGem.PrimalItemResource_FracturedGem,/Game/Extinction/CoreBlueprints/Resources/PrimalItemResource_CorruptedWood.PrimalItemResource_CorruptedWood,/Game/Extinction/CoreBlueprints/Resources/PrimalItemResource_RedSap.PrimalItemResource_RedSap,/Game/Extinction/CoreBlueprints/Resources/PrimalItemResource_ScrapMetal.PrimalItemResource_ScrapMetal,/Game/Extinction/CoreBlueprints/Resources/PrimalItemResource_ScrapMetalIngot.PrimalItemResource_ScrapMetalIngot,/Game/Extinction/CoreBlueprints/Resources/PrimalItemResource_Silicate.PrimalItemResource_Silicate,/Game/Extinction/CoreBlueprints/Resources/PrimalItemResource_ElementRefined.PrimalItemResource_ElementRefined,/Game/Extinction/CoreBlueprints/Resources/PrimalItemResource_ShardRefined.PrimalItemResource_ShardRefined,/Game/PrimalEarth/CoreBlueprints/Weapons/PrimalItemAmmo_RefinedTranqDart.PrimalItemAmmo_RefinedTranqDart,/Game/PrimalEarth/CoreBlueprints/Items/Consumables/PrimalItemConsumable_CookedLambChop.PrimalItemConsumable_CookedLambChop
ShieldHealth=250
GrinderReturnBlockedResources=false
GrinderResourceReturnPercent=33
AdditionalSupportDistanceInFoundations=2
ResourcePullRangeInFoundations=40
GardenerRangeInFoundations=30
FeedingTroughSlotCount=200
LargeStorageSlotCount=250
MetalStorageSlotCount=300
RepairStationSlotCount=1000
SmallStorageSlotCount=150
CryoFridgeSlotCount=300
TekTroughSlotCount=450
VesselSlotCount=100
DisableAbilityToPlaceTeleporterOnSaddles=true
ElementCatalyzerCraftingSpeed=0.005
```
