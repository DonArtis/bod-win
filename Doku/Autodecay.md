# Autodecay Plugin [Link](https://arkserverapi.com/resources/ark-autodecay-anti-autobreed-cluster-support.62/)

Befehle:

`/decaytime` (default) to see how much time is remaining

Administrator Stuff: Nur benutzten wenn du wirklich weißt was du machst !!!

`cheat autodecay.debug` console command for debugging on malfunction

`cheat autodecay.getbp` obtaining a blueprint / asset path for later implementation of the overrides again

`cheat autodecay.forcerun` manually running a check

## Autodecay Gruppen:

Spieler werden automatisch beim ersten joinnen von eine unserer Server in die Gruppe "Default" hinzugefügt (30 Tage)

Die Gruppen mit dem höchsten Zeitraum wird auf einen Spieler angwendet. Selbst wenn dieser in mehreren Gruppen ist
z.B


| Spieler           | Gruppen                 | Effekt                                     |
| ------------------| ----------------------- | -------------------------------------------|
| Spieler1          | Default                 | Strukturen zerfallen nach 30 Tage          |
| Spieler2          | Default,away            | Strukturen zerfallen nach 60 Tage          |
| Spieler3          | decayadmin              | Strukturen werden nicht zerfallen          |
| Spieler3          | Default,decayadmin,away | Strukturen werden nicht zerfallen          |
| Spieler4          |                         | Strukturen zerfallen nach einem Tag        |


"decayadmin": Werden nicht von der Autodecay geprüft

"away": Zerströrung nach 60 Tagen

"": (keiner der oben genanten Gruppen zugehörig) Zerstörung aller Strukturen nach einen Tag

### Befehle

Spieler in den Urlaub schicken(60 Tage):

`Permissions.Add <SteamID> away`

Spieler aus einer Gruppe entfernen:

`Permissions.Remove <SteamID> <Group>`

Spieler Gruppen anzeigen:

`Permissions.PlayerGroups <SteamID>`


Beispiel:
Spieler in den Urlaub schicken:

`Permissions.Add 76561198033596633 away`


---------------------------------------

## Spieler Steamid herausfinden

### Tribeid herausfinden

Cheat HUD aktivieren und Struktur ansehen(geht nicht in Creativmodus `gcm` ):

`setcheatplayer true`

### Spielerliste (Playerid) des Tribes

`GetTribeIdPlayerList TribeID`

### Spielerid zu Steamid

`GetSteamIDForPlayerID Playerid`

-----------------------------------------


## Spieler in Gruppen hinzufügen (Permissions Plugin) [Link](https://arkserverapi.com/resources/ark-permissions.12/)

tbd
