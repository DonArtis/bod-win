#pragma once
#include <API/ARK/Ark.h>
#ifdef ARK_EXPORTS
#define ARK_API __declspec(dllexport)
#else
#define ARK_API __declspec(dllimport)
#endif

namespace AdvancedChat
{
	ARK_API void AddAdvancedChat(const FString& Permission, const FString& Tag, const FLinearColor& NameColour, const FLinearColor& DeadNameColour, const FLinearColor& ChatColour, const FString& ImagePath = "");
	ARK_API void AddAdvancedChat(const uint64& SteamID, const FString& Tag, const FLinearColor& NameColour, const FLinearColor& DeadNameColour, const FLinearColor& ChatColour, const int& WeeklyLimit = 0, const long long& ResetDateMS = 0, const FString& ImagePath = "");
	ARK_API void RemoveAdvancedChat(const FString& Permission);
	ARK_API const FString ReplaceEmojis(FString& Message, const FString& ChatColur);
};
