#!/bin/bash

#
# create needed directories and files for new ansible role
#
# Thanks to Martin S.
# Fork
# 20191106 - v0.1 - maximilian.donhauser@mailbox.org
#
# be carefull, no real sanity checks ;-)

if [ -z "$1" ]; then
  echo "usage: $0 role-name";
  exit 666;
fi

if [ -d "roles/$1" ] ; then
  echo "warning: $1 already exists ... skipping";
  exit 666;
fi

############

# create role directory
mkdir roles/$1 roles/$1/defaults roles/$1/handlers roles/$1/tasks roles/$1/templates
cd roles/$1

# create files
cat >defaults/main.yml <<_EOF
---
# all default variables

_EOF

cat >handlers/main.yml <<_EOF
---
# all needed handlers

_EOF

cat >tasks/main.yml <<_EOF
---
# all needed tasks

_EOF

cat >templates/base.j2 <<_EOF
# {{ ansible_managed }}
#

_EOF


cat >README.md <<_EOF
# usefull role documentation

## preface

## caveats

## etc
_EOF


echo "done ....";
